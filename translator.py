#!/usr/bin/env python3.11

import sys;
try:
    from googletrans import Translator;
except:
    sys.exit("Please install missing library: python3.11 -m pip install --break-system-packages googletrans==4.0.0-rc1");
try:
    import argparse;
    from argparse import RawTextHelpFormatter;
except:
    sys.exit("Please install missing library: python3.11 -m pip install --break-system-packages argparse");

parser = argparse.ArgumentParser(description="Follow the example: \n./translator.py -l pt", formatter_class=RawTextHelpFormatter);
parser.add_argument("--language", "-l", help="""language. ex:
pt(portuguese)
en(english)""", required=True);
args = parser.parse_args();

def traduzir(texto, destino):
    translator = Translator()
    traducao = translator.translate(texto, dest=destino)
    return traducao.text


texto = input("Enter the text to be translated: ")

destination = args.language

result = traduzir(texto, destination)
print("Tradução:", result)
